// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "Week4LabProjectGameMode.generated.h"

UCLASS(minimalapi)
class AWeek4LabProjectGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AWeek4LabProjectGameMode();
};



