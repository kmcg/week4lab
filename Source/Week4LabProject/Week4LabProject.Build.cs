// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class Week4LabProject : ModuleRules
{
	public Week4LabProject(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

		PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "HeadMountedDisplay" });
	}
}
