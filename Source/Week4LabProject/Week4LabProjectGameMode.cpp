// Copyright Epic Games, Inc. All Rights Reserved.

#include "Week4LabProjectGameMode.h"
#include "Week4LabProjectCharacter.h"
#include "UObject/ConstructorHelpers.h"

AWeek4LabProjectGameMode::AWeek4LabProjectGameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/ThirdPersonCPP/Blueprints/ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}
